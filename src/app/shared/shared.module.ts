import { NgModule } from "@angular/core";

import { AppInfoService } from "./app-info.service";
import { UserService } from "./user.service";
import { AuthGuard } from "./auth.guard";

@NgModule({
	providers: [AppInfoService, UserService, AuthGuard],
})

export class AppSharedModule {}
