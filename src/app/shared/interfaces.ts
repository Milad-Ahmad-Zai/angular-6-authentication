
export type TagType = "aurelia" | "angular" | "csharp" | "javascript" | "tools";

export interface User {
	firstName: string;
	lastName: string;
	age: string;
}