import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public isUserLoggedIn: boolean;
  
  constructor() {
    this.isUserLoggedIn = false;
  }

  logIn(model: any) {
    localStorage.setItem('session', JSON.stringify(model));
    this.isUserLoggedIn = true;
  }
  isAuthenticated() {
    return this.isUserLoggedIn;
  }
  getUser() {
    let session = localStorage.getItem('session');
    return session !== null? JSON.parse(session) : null;
  }
  logout() {
    this.isUserLoggedIn = false;
  }

}
