import { Component } from "@angular/core";
import { UserService, User } from "../../shared";

@Component({
	selector: "app-page2",
	templateUrl: "./page2.component.html",
	styleUrls: ["./page2.component.scss"],
})
export class Page2Component {
	user: User;
	constructor(private userService: UserService){}

	ngOnInit(){
		this.user = this.userService.getUser();
	}
}
