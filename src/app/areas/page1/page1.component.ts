import { Component } from "@angular/core";
import { Router } from "@angular/router"
import { User } from "./../../shared/index";
import { UserService } from "../../shared";

@Component({
	selector: "app-page1",
	templateUrl: "./page1.component.html",
	styleUrls: ["./page1.component.scss"],
})

export class Page1Component {
	model: User = {
		firstName: '',
		lastName: '',
		age: ''
	};
	namePattern = "^[a-zA-Z\s]*$";
	agePattern = "^[0-9]*$";

	constructor(private userService: UserService, private router: Router){}

	onSubmit() {
		this.userService.logIn(this.model);
		this.router.navigate(['page2']);
	}
}
